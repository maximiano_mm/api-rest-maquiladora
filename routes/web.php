<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function() use($router)
{
  $router->get('/usuarios/filas/{fila}', ['uses' => 'UsuarioController@inicio']);
  $router->get('/usuarios/empleados', ['uses' => 'UsuarioController@esUsuario']);
  $router->get('/usuarios/{id}', ['uses' => 'UsuarioController@mostrar']);
  $router->post('/usuarios', ['uses' => 'UsuarioController@guardar']);
  $router->post('/usuarios/buscar', ['uses' => 'UsuarioController@buscar']);
  $router->put('/usuarios/{id}', ['uses' => 'UsuarioController@editar']);
  $router->delete('/usuarios/{id}', ['uses' => 'UsuarioController@borrar']);

  // 1 - EMPLEADOS endpoints
  $router->get('/empleados/filas/{fila}', ['uses' => 'EmpleadoController@inicio']);
  $router->get('/empleados/{id}', ['uses' => 'EmpleadoController@mostrar']);
  $router->post('/empleados/buscar', ['uses' => 'EmpleadoController@buscar']);
  $router->post('/empleados', ['uses' => 'EmpleadoController@guardar']);
  $router->put('/empleados/{id}', ['uses' => 'EmpleadoController@editar']);
  $router->delete('/empleados/{id}', ['uses' => 'EmpleadoController@borrar']);

  // 5 - MODELOS endpoints
  $router->get('/modelos/filas/{fila}', ['uses' => 'ModeloController@inicio']);
  $router->get('/modelos/{id}', ['uses' => 'ModeloController@mostrar']);
  $router->post('/modelos/buscar', ['uses' => 'ModeloController@buscar']);
  $router->post('/modelos', ['uses' => 'ModeloController@guardar']);
  $router->put('/modelos/{id}', ['uses' => 'ModeloController@editar']);
  $router->delete('/modelos/{id}', ['uses' => 'ModeloController@borrar']);

  // 8 - OPERACIONES endpoints
  $router->get('/operaciones/filas/{fila}', ['uses' => 'OperacionController@inicio']);
  $router->get('/operaciones/modelos', ['uses' => 'OperacionController@modelos']);
  $router->get('/operaciones/{id}', ['uses' => 'OperacionController@mostrar']);
  $router->post('/operaciones/buscar-operacion', ['uses' => 'OperacionController@buscar_operacion']);
  $router->post('/operaciones/buscar-modelo', ['uses' => 'OperacionController@buscar_modelo']);
  $router->post('/operaciones', ['uses' => 'OperacionController@guardar']);
  $router->put('/operaciones/{id}', ['uses' => 'OperacionController@editar']);
  $router->delete('/operaciones/{id}', ['uses' => 'OperacionController@borrar']);

  // 6 - MODELOS LOTES endpoints
  $router->get('/lotes/filas/{fila}', ['uses' => 'ModeloLoteController@inicio']);
  $router->get('/lotes/modelos', ['uses' => 'ModeloLoteController@modelos']);
  $router->get('/lotes/{id}', ['uses' => 'ModeloLoteController@mostrar']);
  $router->post('/lotes/buscar', ['uses' => 'ModeloLoteController@buscar']);
  $router->post('/lotes', ['uses' => 'ModeloLoteController@guardar']);
  $router->put('/lotes/{id}', ['uses' => 'ModeloLoteController@editar']);
  $router->delete('/lotes/{id}', ['uses' => 'ModeloLoteController@borrar']);

  // 13 - NOMINAS endpoints
  $router->get('/nominas/filas/{fila}', ['uses' => 'NominaController@inicio']);
  $router->get('/nominas/{id}', ['uses' => 'NominaController@mostrar']);
  $router->post('/nominas/buscar', ['uses' => 'NominaController@buscar']);
  $router->post('/nominas', ['uses' => 'NominaController@guardar']);
  $router->put('/nominas/{id}', ['uses' => 'NominaController@editar']);
  $router->delete('/nominas/{id}', ['uses' => 'NominaController@borrar']);

  // 14 - NOMINAS ITEMS endpoints
  $router->get('/nomina-items/filas/{fila}', ['uses' => 'NominaItemController@inicio']);
  $router->get('/nomina-items/{id}', ['uses' => 'NominaItemController@mostrar']); // NOTE: ESTA RUTA ES NECESARIA???
  $router->post('/nomina-items', ['uses' => 'NominaItemController@guardar']);
  $router->put('/nomina-items/{id}', ['uses' => 'NominaItemController@editar']);
  $router->delete('/nomina-items/{id}', ['uses' => 'NominaItemController@borrar']);

  // 7 - DEFECTOS endpoints
  $router->get('/defectos/filas/{fila}', ['uses' => 'DefectoController@inicio']);
  $router->get('/defectos/modelos', ['uses' => 'DefectoController@modelos']);
  $router->get('/defectos/empleados', ['uses' => 'DefectoController@empleados']);
  $router->get('/defectos/{id}', ['uses' => 'DefectoController@mostrar']);
  $router->post('/defectos/buscar_empleado', ['uses' => 'DefectoController@buscar_empleado']);
  $router->post('/defectos/buscar_modelo', ['uses' => 'DefectoController@buscar_modelo']);
  $router->post('/defectos', ['uses' => 'DefectoController@guardar']);
  $router->put('/defectos/{id}', ['uses' => 'DefectoController@editar']);
  $router->delete('/defectos/{id}', ['uses' => 'DefectoController@borrar']);

  // 3 - TELAS endpoints
  $router->get('/telas/filas/{fila}', ['uses' => 'TelaController@inicio']);
  $router->get('/telas/{id}', ['uses' => 'TelaController@mostrar']);
  $router->post('/telas/buscar', ['uses' => 'TelaController@buscar']);
  $router->post('/telas', ['uses' => 'TelaController@guardar']);
  $router->put('/telas/{id}', ['uses' => 'TelaController@editar']);
  $router->delete('/telas/{id}', ['uses' => 'TelaController@borrar']);

  // 4 - PROVEEDORES endpoints
  $router->get('/proveedores/filas/{fila}', ['uses' => 'ProveedorController@inicio']);
  $router->get('/proveedores/{id}', ['uses' => 'ProveedorController@mostrar']);
  $router->post('/proveedores/buscar', ['uses' => 'ProveedorController@buscar']);
  $router->post('/proveedores', ['uses' => 'ProveedorController@guardar']);
  $router->put('/proveedores/{id}', ['uses' => 'ProveedorController@editar']);
  $router->delete('/proveedores/{id}', ['uses' => 'ProveedorController@borrar']);

  // 9 - ADQUISICIONES endpoints
  $router->get('/adquisiciones/filas/{fila}', ['uses' => 'AdquisicionController@inicio']); // NOTE: VERIFICAR ESTA RUTA
  $router->get('/adquisiciones/proveedores', ['uses' => 'AdquisicionController@proveedores']);
  $router->get('/adquisiciones/{id}', ['uses' => 'AdquisicionController@mostrar']);
  $router->post('/adquisiciones/buscar', ['uses' => 'AdquisicionController@buscar']);
  $router->post('/adquisiciones', ['uses' => 'AdquisicionController@guardar']);
  $router->put('/adquisiciones/{id}', ['uses' => 'AdquisicionController@editar']);
  $router->delete('/adquisiciones/{id}', ['uses' => 'AdquisicionController@borrar']);

  // 10 - ADQUISICIONES ITEMS endpoints
  $router->get('/adquisicion-items/filas/{fila}', ['uses' => 'AdquisicionItemController@inicio']);
  $router->get('/adquisicion-items/telas', ['uses' => 'AdquisicionController@proveedores']);
  $router->get('/adquisicion-items/{id}', ['uses' => 'AdquisicionItemController@mostrar']); // NOTE: VERIFICAR SI SE USARA ESTA RUTA
  $router->post('/adquisicion-items', ['uses' => 'AdquisicionItemController@guardar']);
  $router->put('/adquisicion-items/{id}', ['uses' => 'AdquisicionItemController@editar']);
  $router->delete('/adquisicion-items/{id}', ['uses' => 'AdquisicionItemController@borrar']);

  // 2 - CLIENTES endpoints
  $router->get('/clientes/filas/{fila}', ['uses' => 'ClienteController@inicio']);
  $router->get('/clientes/{id}', ['uses' => 'ClienteController@mostrar']);
  $router->post('/clientes/buscar', ['uses' => 'ClienteController@buscar']);
  $router->post('/clientes', ['uses' => 'ClienteController@guardar']);
  $router->put('/clientes/{id}', ['uses' => 'ClienteController@editar']);
  $router->delete('/clientes/{id}', ['uses' => 'ClienteController@borrar']);

  // 11 - VENTAS endpoints
  $router->get('/ventas/filas/{fila}', ['uses' => 'VentaController@inicio']);
  $router->get('/ventas/{id}', ['uses' => 'VentaController@mostrar']);
  $router->post('/ventas/buscar', ['uses' => 'VentaController@buscar']);
  $router->post('/ventas', ['uses' => 'VentaController@guardar']);
  $router->put('/ventas/{id}', ['uses' => 'VentaController@editar']);
  $router->delete('/ventas/{id}', ['uses' => 'VentaController@borrar']);

  // 12 - VENTAS ITEMS endpoints
  $router->get('/venta-items/filas/{fila}', ['uses' => 'VentaItemController@inicio']); // NOTE: VERIFICAR ESTA RUTA
  $router->get('/venta-items/{id}', ['uses' => 'VentaItemController@mostrar']); // NOTE: ESTA RUTA ES NCESARIA???
  $router->post('/venta-items', ['uses' => 'VentaItemController@guardar']);
  $router->put('/venta-items/{id}', ['uses' => 'VentaItemController@editar']);
  $router->delete('/venta-items/{id}', ['uses' => 'VentaItemController@borrar']);
});
