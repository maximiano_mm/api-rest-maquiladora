<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ModeloLote;
use App\Models\Modelo;

class ModeloLoteController extends Controller
{
  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $lotes = ModeloLote::with('modelo')->paginate($fila);
    return response()->json($lotes, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $lote = ModeloLote::find($id);
    return response()->json($lote, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $modelo = Modelo::where('nombre', 'LIKE', "%$busqueda%")->get();
    $resultado = ModeloLote::where('modelo_id', 'LIKE', $modelo[0]['id'])->paginate(10);
    return response()->json($resultado, 200);
  }

  // Lista de todos los modelos
  public function modelos() {
    $modelos = Modelo::all();
    return response()->json($modelos, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    ModeloLote::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    ModeloLote::where('id', $id)->update($this->capturar($request));
    // $lote = ModeloLote::findOrFail($id);
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    ModeloLote::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar el registro
  public function validar($lote)
  {
    return $this->validate($lote, [
      'cantidad'    => 'bail|required|integer|digits_between:1,5',
      'descripcion' => 'required|string|min:10|max:256',
      'modelo_id'   => 'required|integer|digits_between:1,5'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return[
      'cantidad'    => $datos->input('cantidad'),
      'descripcion' => $datos->input('descripcion'),
      'modelo_id'   => $datos->input('modelo_id')
    ];
  }
}
