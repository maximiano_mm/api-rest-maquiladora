<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NominaItem;

class NominaItemController extends Controller
{
  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($id)
  {
    $items = NominaItem::with(['nomina', 'modelo'])->get();
    return response()->json($items, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $resultado = NominaItem::where('item', 'LIKE', "%$busqueda%")->paginate(10);
    return response()->json($resultado, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    NominaItem::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    NominaItem::where('id', $id)->update($this->capturar($request));
    // $iten = NominaItem::findOrFail($id);
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    NominaItem::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar/editar el registro
  public function validar($item)
  {
    return $this->validate($item, [
      'cantidad'  => 'bail|required|integer|min:1|max:20',
      'precio'    => 'required|integer',
      'nomina_id' => 'required|integer|min:1|max:20',
      'modelo_id' => 'required|integer|min:1|max:20'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'cantidad'  => $datos->input('cantidad'),
      'precio'    => $datos->input('precio'),
      'nomina_id' => $datos->input('nomina_id'),
      'modelo_id' => $datos->input('modelo_id')
    ];
  }
}
