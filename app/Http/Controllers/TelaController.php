<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tela;

class TelaController extends Controller
{
  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $telas = Tela::paginate($fila);
    return response()->json($telas, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $tela = Tela::find($id);
    return response()->json($tela, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $resultado = Tela::where('nombre', 'LIKE', "%$busqueda%")->paginate(10);
    return response()->json($resultado, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    Tela::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    Tela::where('id', $id)->update($this->capturar($request));
    // $tela = Tela::findOrFail($id);
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    $tela = Tela::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar/editar el registro
  public function validar($tela)
  {
    return $this->validate($tela, [
      'nombre'          => 'bail|required|string|min:5|max:20',
      'color'           => 'required|alpha_dash|min:3|max:20',
      'caracteristicas' => 'required|string|min:10|max:256',
      'seccion'         => 'required|string|min:5|max:20',
      'precio'          => 'required|numeric'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'nombre'          => $datos->input('nombre'),
      'color'           => $datos->input('color'),
      'caracteristicas' => $datos->input('caracteristicas'),
      'seccion'         => $datos->input('seccion'),
      'precio'          => $datos->input('precio')
    ];
  }
}
