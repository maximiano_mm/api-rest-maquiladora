<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdquisicionItem;

class AdquisicionItemController extends Controller
{

  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($filas)
  {
    $items = AdquisicionItem::with(['adquisicion', 'tela'])->paginate($filas);
    return response()->json($items, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    AdquisicionItem::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    AdquisicionItem::where('id', $id)->update($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    AdquisicionItem::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar/editar el registro
  public function validar($item)
  {
    return $this->validate($item, [
      'cantidad'               => 'bail|required|integer|min:1|max:20',
      'precio'                 => 'required|numeric',
      'adquisicion_id'         => 'required|integer|min:1|max:20',
      'tela_id'                => 'required|integer|min:1|max:20'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'cantidad'               => $datos->input('cantidad'),
      'precio'                 => $datos->input('precio'),
      'adquisicion_id'         => $datos->input('adquisicion_id'),
      'tela_id'                => $datos->input('tela_id')
    ];
  }
}
