<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Adquisicion;
use App\Models\Proveedor;

class AdquisicionController extends Controller
{

  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $adquisiciones = Adquisicion::with(['proveedor', 'items'])->paginate($fila);
    return response()->json($adquisiciones, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $adquisicion = Adquisicion::find($id);
    return response()->json($adquisicion, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $proveedor = Proveedor::where('nombre', 'LIKE', "%$busqueda%")->get();
    $resultado = Adquisicion::where('proveedor_id', 'LIKE', $proveedor[0]['id'])->paginate(10);
    return response()->json($resultado, 200);
  }

  /**
   * Lista de proveedores
   * @return \Illuminate\Http\Response
   */
  public function proveedores() {
    $proveedores = Proveedor::all();
    return response()->json($proveedores, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    Adquisicion::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    $adquisicion = Adquisicion::where('id', $id)->update($this->capturar($request));
    return response()->json($adquisicion, 201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    Adquisicion::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar/editar el registro
  public function validar($adquisicion)
  {
    return $this->validate($adquisicion, [
      'proveedor_id' => 'bail|required|integer|min:1|max:20',
      'descripcion'  => 'required|string|min:10|max:256',
      'total'        => 'required|numeric',
      'estado'       => 'required|boolean'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'proveedor_id' => $datos->input('proveedor_id'),
      'descripcion'  => $datos->input('descripcion'),
      'total'        => $datos->input('total'),
      'estado'       => $datos->input('estado')
    ];
  }
}
