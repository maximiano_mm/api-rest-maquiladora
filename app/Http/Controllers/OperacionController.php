<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Operacion;
use App\Models\Modelo;

class OperacionController extends Controller
{
  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $operaciones = Operacion::with('modelo')->paginate($fila);
    return response()->json($operaciones, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $operacion = Operacion::find($id);
    return response()->json($operacion, 200);
  }

  // Lista las operaciones
  public function buscar_operacion(Request $request)
  {
    $busqueda = $request->input('search');
    $resultado = Operacion::where('nombre', 'LIKE', "%$busqueda%")->paginate(10);
    return response()->json($resultado, 200);
  }

  // Lista las operaciones por modelo
  public function buscar_modelo(Request $request)
  {
    $busqueda = $request->input('search');
    $modelo = Modelo::where('nombre', 'LIKE', "%$busqueda%")->get();
    $resultado = Operacion::where('modelo_id', 'LIKE', $modelo[0]['id'])->paginate(10);
    return response()->json($resultado, 200);
  }

  // Lista todos los modelos
  public function modelos() {
    $modelos = Modelo::all();
    return response()->json($modelos, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validarAlGuardar($request);
    Operacion::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validarAlEditar($request);
    Operacion::where('id', $id)->update($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    Operacion::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar el registro
  public function validarAlGuardar($operacion)
  {
    return $this->validate($operacion, [
      'nombre'    => 'bail|unique:operaciones|required|string|min:5|max:20',
      'precio'    => 'required|numeric',
      'maquina'   => 'required|string|min:5|max:20',
      'modelo_id' => 'required|integer|min:1|max:20'
    ]);
  }
  // Valida los campos al editar el registro
  public function validarAlEditar($operacion)
  {
    return $this->validate($operacion, [
      'nombre'    => 'bail|required|string|min:5|max:20',
      'precio'    => 'required|numeric',
      'maquina'   => 'required|string|min:5|max:20',
      'modelo_id' => 'required|integer|min:1|max:20'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'nombre'    => $datos->input('nombre'),
      'precio'    => $datos->input('precio'),
      'maquina'   => $datos->input('maquina'),
      'modelo_id' => $datos->input('modelo_id')
    ];
  }
}
