<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Venta;
use App\Models\Cliente;

class VentaController extends Controller
{
  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $ventas = Venta::with(['cliente', 'items'])->paginate($fila);
    return response()->json($ventas, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $venta = Venta::find($id);
    return response()->json($venta, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $cliente = Cliente::where('nombre', 'LIKE', "%$busqueda%")->get();
    $resultado = Venta::where('cliente_id', 'LIKE', "%" . $cliente[0]['id'] . "%")->paginate(10);
    return response()->json($resultado, 200);
    //dd($resultado);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    Venta::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    Venta::where('id', $id)->update($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    Venta::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar/editar el registro
  public function validar($venta)
  {
    return $this->validate($venta, [
      'cliente_id'  => 'bail|required|integer|min:1|max:20',
      'descripcion' => 'required|string|min:10|max:256',
      'total'       => 'required|numeric',
      'estado'      => 'required|boolean'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'cliente_id'  => $datos->input('cliente_id'),
      'descripcion' => $datos->input('descripcion'),
      'total'       => $datos->input('total'),
      'estado'      => $datos->input('estado')
    ];
  }
}
