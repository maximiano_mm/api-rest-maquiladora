<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proveedor;

class ProveedorController extends Controller
{
  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $proveedores = Proveedor::paginate($fila);
    return response()->json($proveedores, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $proveedor = Proveedor::find($id);
    return response()->json($proveedor, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $resultado = Proveedor::where('nombre', 'LIKE', "%$busqueda%")->paginate(10);
    return response()->json($resultado, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validarAlGuardar($request);
    Proveedor::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validarAlEditar($request);
    Proveedor::where('id', $id)->update($this->capturar($request));
    // $proveedor = Proveedor::findOrFail($id);
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    Proveedor::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar el registro
  public function validarAlGuardar($cliente)
  {
    return $this->validate($cliente, [
      'nombre'     => 'bail|required|string|min:3|max:20',
      'apellido_p' => 'required|string|min:3|max:15',
      'apellido_m' => 'required|string|min:3|max:15',
      'correo'     => 'unique:clientes|required|email|min:12|max:30',
      'telefono'   => 'unique:clientes|required|alpha_dash|min:10|max:15'
    ]);
  }

  // Valida los campos al editar el registro
  public function validarAlEditar($cliente)
  {
    return $this->validate($cliente, [
      'nombre'     => 'bail|required|string|min:3|max:20',
      'apellido_p' => 'required|string|min:3|max:15',
      'apellido_m' => 'required|string|min:3|max:15',
      'correo'     => 'required|email|min:12|max:30',
      'telefono'   => 'required|alpha_dash|min:10|max:15'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'nombre'     => $datos->input('nombre'),
      'apellido_p' => $datos->input('apellido_p'),
      'apellido_m' => $datos->input('apellido_m'),
      'correo'     => $datos->input('correo'),
      'telefono'   => $datos->input('telefono')
    ];
  }
}
