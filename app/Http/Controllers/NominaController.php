<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nomina;
use App\Models\Empleado;

class NominaController extends Controller
{
  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $nominas = Nomina::with(['empleado', 'items'])->paginate($fila);
    return response()->json($nominas, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $nomina = Nomina::find($id);
    return response()->json($nomina, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $empleado = Empleado::where('nombre', 'LIKE', "%$busqueda%")->get();
    $resultado = Nomina::where('empleado_id', 'LIKE', $empleado[0]['id'])->paginate(10);
    return response()->json($resultado, 200);
    // dd($resultado);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    Nomina::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    Nomina::where('id', $id)->update($this->capturar($request));
    // $nomina = Nomina::findOrFail($id);
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    Nomina::where('id', '=', $id)->delete();
    return response()->json(200);
  }

    // Valida los campos al insertar/editar el registro
  public function validar($nomina)
  {
    return $this->validate($nomina, [
      'empleado_id' => 'bail|required|integer|min:1|max:20',
      'descripcion' => 'required|string|min:10|max:256',
      'total'       => 'required|numeric',
      'estado'      => 'required|boolean'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'empleado_id' => $datos->input('empleado_id'),
      'descripcion' => $datos->input('descripcion'),
      'total'       => $datos->input('total'),
      'estado'      => $datos->input('estado')
    ];
  }
}
