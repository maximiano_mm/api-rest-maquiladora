<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Modelo;

class ModeloController extends Controller
{

  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $modelos = Modelo::paginate($fila);
    return response()->json($modelos, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $modelo = Modelo::find($id);
    return response()->json($modelo, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $resultado = Modelo::where('nombre', 'LIKE', "%$busqueda%")->paginate(10);
    return response()->json($resultado, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validarAlGuardar($request);
    Modelo::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validarAlEditar($request);
    Modelo::where('id', $id)->update($this->capturar($request));
    // $modelo = Modelo::findOrFail($d);
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    Modelo::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar el registro
  public function validarAlGuardar($modelo)
  {
    return $this->validate($modelo, [
      'nombre'      => 'bail|unique:modelos|required|string|min:5|max:20',
      'descripcion' => 'required|string|min:10|max:256',
      'tipo'        => 'required|string|min:5|max:20',
      'talla'       => 'required|string|min:5|max:15',
      'precio'      => 'required|numeric'
    ]);
  }

  // Valida los campos al editar el registro
  public function validarAlEditar($modelo)
  {
    return $this->validate($modelo, [
      'nombre'      => 'bail|required|string|min:5|max:20',
      'descripcion' => 'required|string|min:10|max:256',
      'tipo'        => 'required|string|min:5|max:20',
      'talla'       => 'required|string|min:5|max:15',
      'precio'      => 'required|numeric'
    ]);
  }

  public function capturar($datos)
  {
    return [
      'nombre'      => $datos->input('nombre'),
      'descripcion' => $datos->input('descripcion'),
      'tipo'        => $datos->input('tipo'),
      'talla'       => $datos->input('talla'),
      'precio'      => $datos->input('precio')
    ];
  }
}
