<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VentaItem;

class VentaItemController extends Controller
{
  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function inicio($id)
  {
    $items = VentaItem::with(['venta', 'modelo'])->get();
    return response()->json($items, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $resultado = VentaItem::where('item', 'LIKE', "%$busqueda%")->paginate(10);
    return response()->json($resultado, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    VentaItem::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    VentaItem::where('id', $id)->update($this->capturar($request));
    //$item = VentaItem::findOrFail($id);
    return response()->json(201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    VentaItem::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar/editar el registro
  public function validar($item)
  {
    return $this->validate($item, [
      'cantidad'         => 'bail|required|integer|min:1|max:20',
      'precio'           => 'required|numeric',
      'venta_id'         => 'required|integer|min:1|max:20',
      'modelo_id'        => 'required|integer|min:1|max:20'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'cantidad'         => $datos->input('cantidad'),
      'precio'           => $datos->input('precio'),
      'venta_id'         => $datos->input('venta_id'),
      'modelo_id'        => $datos->input('modelo_id')
    ];
  }
}
