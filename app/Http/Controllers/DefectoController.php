<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Defecto;
use App\Models\Modelo;
use App\Models\Empleado;

class DefectoController extends Controller
{

  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $defectos = Defecto::with(['empleado', 'modelo'])->paginate($fila);
    return response()->json($defectos, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $defecto = Defecto::find($id);
    return response()->json($defecto, 200);
  }

  /**
   * Realiza un listado de defectos por modelo
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar_modelo(Request $request)
  {
    $busqueda = $request->input('search');
    $modelo = Modelo::where('nombre', 'LIKE', "%$busqueda%")->get();
    $resultado = Defecto::where('modelo_id', 'LIKE', "%" . $modelo[0]['id'] ."%")->paginate(10);
    return response()->json($resultado, 200);
  }

  /**
   * Realiza un listado de defectos por empleado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar_empleado(Request $request)
  {
    $busqueda = $request->input('search');
    $empleado = Empleado::where('nombre', 'LIKE', "%$busqueda%")->get();
    $resultado = Defecto::where('empleado_id', 'LIKE', "%" . $empleado[0]['id'] ."%")->paginate(10);
    return response()->json($resultado, 200);
  }

  // Listado de los modelos
  public function modelos() {
    $modelos = Modelo::all();
    return response()->json($modelos, 200);
  }

  // Listadp de los empleados
  public function empleados() {
    $empleados = Empleado::all();
    return response()->json($empleados, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validar($request);
    Defecto::create($this->capturar($request));
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validar($request);
    $defecto = Defecto::where('id', $id)->update($this->capturar($request));
    return response()->json($defecto, 201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    Defecto::where('id', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar el registro
  public function validar($defecto)
  {
    return $this->validate($defecto, [
      'cantidad'    => 'bail|required|integer|digits_between:1,5',
      'descripcion' => 'required|string|min:10|max:256',
      'empleado_id' => 'required|integer|digits_between:1,5',
      'modelo_id'   => 'required|integer|digits_between:1,5',
    ]);
  }

  public function capturar($datos)
  {
    return [
      'cantidad'    => $datos->input('cantidad'),
      'descripcion' => $datos->input('descripcion'),
      'empleado_id' => $datos->input('empleado_id'),
      'modelo_id'   => $datos->input('modelo_id')
    ];
  }
}
