<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Empleado;

class UsuarioController extends Controller
{
  /**
   * Muestra el listado seccionado del recurso
   * @param int $fila
   * @return \Illuminate\Http\Response
   */
  public function inicio($fila)
  {
    $usuarios = Usuario::with('empleado')->paginate($fila);
    return response()->json($usuarios, 200);
  }

  /**
   * Muestra un elemento en específico
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function mostrar($id)
  {
    $usuario = Usuario::find($id);
    return response()->json($usuario, 200);
  }

  /**
   * Realiza una busqueda por caracter ingresado
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function buscar(Request $request)
  {
    $busqueda = $request->input('search');
    $resultado = Usuario::where('usuario', 'LIKE', "%$busqueda%")->paginate(10);
    return response()->json($resultado, 200);
  }

  // DEvuelve si el empleado es usuario o no
  public function esUsuario()
  {
    $empleados = Empleado::where('es_usuario', 'LIKE', 'no')->get();
    return response()->json($empleados, 200);
  }

  /**
   * Almaceda un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function guardar(Request $request)
  {
    $this->validarAlGuardar($request);
    Usuario::create($this->capturar($request));
    // $empleado = Empleado::findOrFail($request->empleado_id);
    // $empleado->es_usuario = 1;
    // $empleado->update($empleado->all());
    return response()->json(201);
  }

  /**
   * Actualiza un recurso
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function editar($id, Request $request)
  {
    $this->validarAlEditar($request);
    Usuario::where('id', $id)->update($this->capturar($request));
    // $usuario = Usuario::findOrFail($id)
    return response()->json($usuario, 201);
  }

  /**
   * Remueve un recurso
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function borrar($id)
  {
    Usuario::where('id', '=', $id)->delete();
    return response()->json(200);
  }

  // Valida los campos al insertar el registro
  public function validarAlGuardar($usuario)
  {
    return $this->validate($usuario, [
      'usuario'     => 'bail|unique:usuarios|required|string|min:5|max:20',
      'contrasena'  => 'required|alpha_dash|min:8|max:20',
      'rol'         => 'required',
      'empleado_id' => 'required|integer|min:1|max:20'
    ]);
  }

  // Valida los campos al editar el registro
  public function validarAlEditar($usuario)
  {
    return $this->validate($usuario, [
      'usuario'     => 'bail|required|string|min:5|max:20',
      'contrasena'  => 'required|alpha_dash|min:8|max:20',
      'rol'         => 'required',
      'empleado_id' => 'required|integer|min:1|max:20'
    ]);
  }

  // Captura los datos del request
  public function capturar($datos)
  {
    return [
      'usuario'     => $datos->input('usuario'),
      'contrasena'  => $datos->input('contrasena'),
      'rol'         => $datos->input('rol'),
      'empleado_id' => $datos->input('empleado_id')
    ];
  }
}
