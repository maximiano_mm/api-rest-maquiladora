<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operacion extends Model
{
  protected $table = 'operaciones';

  /**
   * The attributes that are mass assignable.
   * @var array
   */
  protected $fillable = [
    'nombre',
    'precio',
    'maquina',
    'modelo_id'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   * @var array
   */
  protected $hidden = [

  ];

  public function modelo()
  {
    return $this->belongsTo(Modelo::class);
  }
}
?>
