<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NominaItem extends Model
{
  protected $table = 'nominas_items';

  /**
   * The attributes that are mass assignable.
   * @var array
   */
  protected $fillable = [
    'cantidad',
    'precio',
    'nomina_id',
    'modelo_id'

    ];

  /**
   * The attributes excluded from the model's JSON form.
   * @var array
   */
  protected $hidden = [

  ];

  public function nomina()
  {
    return $this->belongsTo(Nomina::class);
  }

  public function modelo()
  {
    return $this->belongsTo(Modelo::class);
  }
}
?>
