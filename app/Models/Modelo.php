<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
  protected $table = 'modelos';
  
  /**
   * The attributes that are mass assignable.
   * @var array
   */
  protected $fillable = [
    'imagen',
    'nombre',
    'descripcion',
    'tipo',
    'talla',
    'precio'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   * @var array
   */
  protected $hidden = [

  ];

  public function nominaItems()
  {
    return $this->hasMany(NominaItem::class);
  }

  public function ventaItems()
  {
    return $this->hasMany(VentaItem::class);
  }

  public function defectos()
  {
    return $this->hasMany(Defecto::class);
  }

  public function lotes()
  {
    return $this->hasMany(ModeloLote::class);
  }

  public function operaciones()
  {
    return $this->hasMany(Operacion::class);
  }
}
?>
